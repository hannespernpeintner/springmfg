package de.hanno.springmfg.controller;

import de.hanno.springmfg.model.User;
import de.hanno.springmfg.model.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;

@Controller
public class IndexController {

    @Inject
    UserRepository userRepository;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("message", "xxxxxxxxxxxxxx!");
        model.addAttribute("userCount", userRepository.findAll().size());
        return "index";
    }
}
