package de.hanno.springmfg.controller;

import de.hanno.springmfg.model.User;
import de.hanno.springmfg.model.UserRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.function.Consumer;

@RestController
public class UserController {

    @Inject
    UserRepository userRepository;

    @RequestMapping(value="/user/{userName}", method = RequestMethod.POST)
    User add(@PathVariable String userName) {
        return this.userRepository.save(new User(userName));
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public User readUser(@PathVariable Long userId) {
        return userRepository.findById(userId);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> readUsers() {
        return userRepository.findAll();
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class UserNotFoundException extends RuntimeException {

        public UserNotFoundException(Long userId) {
            super("could not find user '" + userId + "'.");
        }
    }

}
