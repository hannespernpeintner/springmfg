package de.hanno.springmfg.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface UserRepository extends Repository<User, Long> {
    <S extends User> S save(S user);

    Page<User> findAll(Pageable pageable);
    List<User> findAll();

    User findById(Long userId);
    User findByNameAllIgnoringCase(String name);

}
