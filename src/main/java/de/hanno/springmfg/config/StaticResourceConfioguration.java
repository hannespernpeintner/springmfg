package de.hanno.springmfg.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//@Configuration
class StaticResourceConfiguration extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        //registry.addResourceHandler("/index").addResourceLocations("/resources/static/index.html");
        //registry.addResourceHandler("/js/**").addResourceLocations("/resources/static/js");
        //registry.addResourceHandler("/css/**").addResourceLocations("/resources/static/css");
    }
}