package de.hanno.springmfg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {

        //EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        //EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.H2).addScript("my-schema.sql").addScript("my-test-data.sql").build();
        // do stuff against the db (EmbeddedDatabase extends javax.sql.DataSource)
        //db.shutdown();
        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
    }
}
