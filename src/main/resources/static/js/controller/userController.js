angular.module('mfg', [])
  .controller('user', function($scope, $http) {
    $http.get('/users').
      success(function(data, status, headers, config) {
        console.log(data);
        $scope.users = data;
      }).
      error(function(data, status, headers, config) {
        console.log("users controller error");
      });
})